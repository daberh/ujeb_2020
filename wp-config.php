<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ujeb_2020' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9/vZoi`?X`mz*uX~[HEYvRT~}LKNVmY3xHp/O7+*{btqBH{a uQOs]KM9:y]S>9,' );
define( 'SECURE_AUTH_KEY',  'G/>q)*V3p}ytB6;WzN(hxQT/&/P rl2&G~9P7ac~4?4qGD9uY@7r*XfZx%&)}QVz' );
define( 'LOGGED_IN_KEY',    'GnC}NFnmO$Z`<{$BDluNB=:&7!=I?z],vQMl0KvBZ#h0vq7j<07u2yJP?pt`8|K!' );
define( 'NONCE_KEY',        '?q0+/0cBYG6Q6wrc<+.SC9j<SKc61@T)hv|}fM6+kqnVxR;ZxFB,SZquAWPSh%NE' );
define( 'AUTH_SALT',        '} 25-.[u)^=#y(TYurw!%xU{v<H,]X }E11PQy9q2cEc56>Z(Z#@4yN~X0GZP:`M' );
define( 'SECURE_AUTH_SALT', 'dL]1FL?|Mv|>cxwDl;5n/YGJGOuQH/^)wo{Vlou#:.y!x@-Tr*rn[-n}boyU0NQ~' );
define( 'LOGGED_IN_SALT',   'amK`X1XxrZdib3 n`l&S:ys|N_?%:wIT4wZamqnN&S`D4}Nih].o{{.>+,2;kECI' );
define( 'NONCE_SALT',       'KRxD*ZK}2#=Y9%-i6vo+a;[]:PEfZM+FV>RN||}4zsu;MQ]09pt@U1T?;Ei@H9.?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ujeb2020_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', true );

define('WPCF7_AUTOP', false);


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
