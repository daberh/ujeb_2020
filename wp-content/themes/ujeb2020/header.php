<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="Author" content="ujeb.be">
	<meta name="Copyright" content="ujeb.be">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<?php wp_head() ?>
	<!--[if LTE IE 8]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script>var home_url = '<?= get_home_url() ?>';</script>
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

</head>
<body <?php body_class() ?>>
<header id="home">
	<?php if(ujeb2020_display_preloader()): ?>
	<div class="preloader"><div class="preloader-wrapper">
		<img src="<?= get_stylesheet_directory_uri() ?>/assets/media/logos/ujeb-logo.svg" alt="ajax-loader">
	</div></div>
	<?php endif; ?>
	<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="primary-menu">
		<div class="container">
			<a class="navbar-brand" href="<?= get_home_url() ?>"><img id="logo" src="<?= get_stylesheet_directory_uri() ?>/assets/media/logos/ujeb-logo.svg" alt=""></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse flex-column" id="navbarResponsive">
				<ul class="navbar-nav follow-us-top-nav ml-auto">
					<li class="follow-us-text-top grandhotel"><?= ___('nous suivre') ?> </li>
				<?php
				$seo_data = get_option( 'wpseo_social' );
					if (!empty($seo_data['facebook_site'])) : ?><li class="nav-item"><a target="_blank" href="<?= $seo_data['facebook_site'] ?>" class="nav-link"><i class="fab fa-facebook fa-2x"></i></a></li><?php
					endif;
					if (!empty($seo_data['twitter_site'])) : ?><li class="nav-item"><a target="_blank" href="https://twitter.com/<?= $seo_data['twitter_site'] ?>" class="nav-link"><i class="fab fa-twitter fa-2x"></i></a></li><?php
					endif;
					if (!empty($seo_data['youtube_url'])) : ?><li class="nav-item"><a target="_blank" href="<?= $seo_data['youtube_url'] ?>" class="nav-link"><i class="fab fa-youtube fa-2x"></i></a></li><?php
					endif;
				?>
				</ul>
				<ul class="navbar-nav ml-auto real-nav">
					<?php
					$items = wp_get_nav_menu_items(ujeb2020_get_primary_nav_menu_id());
					foreach ($items as $item)
						echo '<li class="nav-item"><a class="nav-link" href="'.$item->url.'">'.$item->title.'</a></li>';
					?>
				</ul>
				<?php if(is_front_page()): ?>
					<script>
						jQuery(document).ready(function($) {
							$('.real-nav .nav-link').first().attr('href', '#home');
						});
					</script>
				<?php endif; ?>
			</div>
		</div>
	</nav>
<?php if(is_front_page())get_template_part('partials/slider'); ?>
</header>