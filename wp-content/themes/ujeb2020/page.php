<?php
get_header();

?>
<section class="py-5">
	<div class="container">
		<p class="py-3">&nbsp;</p>
		<h1 class="py-5 display-4000 text-center"><?= get_the_title() ?></h1>
		<div class="user-input lead">
			<?php if (have_posts()):while (have_posts()):the_post();the_content();endwhile;endif;?>
		</div>
	</div>
</section>


<?php
get_footer();
?>