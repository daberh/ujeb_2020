<?php


$textDomain = 'ujeb2020';

function ___($entry){
	global $textDomain;
	return __($entry, $textDomain);
}

$functionsFiles = [
	'setup',
	'config',
	'cpt',
	'admin_custom',
	'custom_wpcf7',
	
];
$classes = ['classes'];

$dirClasses= 'includes';
$dirSeparator = '/';
$dirFunctions = 'includes';

foreach( $classes as $c )
	@require_once $dirClasses . $dirSeparator . $c .'.php';

foreach( $functionsFiles as $file )
	@require_once $dirFunctions . $dirSeparator . $file .'.php';

