<section class="py-5" id="events">
	<div class="container">
		<h1 class="my-4 text-center"><?= get_field('titre_evenements') ?></h1>
		<div class="row">
		<?php
			$events = new WP_Query([
				'post_type' => 'ujeb_events',
				'order' => 'ASC',
				'orderby' => 'meta_value_num',
				'meta_key' => 'start_date',
				'meta_query' => [
					[
						'key' => 'start_date',
						'value' => date('Ymd'),
						'type' => 'DATE',
						'compare' => '>='
					]
				]
			]);
			if($events->have_posts()): $nombre = 1;
				while($events->have_posts()):
					$events->the_post();
					$format = sanitize_title(get_post_format_string(get_post_format()));
					
					if ($nombre%2 == 1)
						$anim = 'fade-right';
					else
						$anim = 'fade-left';
					set_query_var( 'animation', $anim );
					set_query_var( 'animation-time', '' );
					get_template_part( 'partials/event-formats/'.$format);
					$nombre++;
				endwhile;
			else:
			?><p class="text-center col-12">rien pour l'instant</p><?php
			endif;
			wp_reset_postdata();
			?>
		</div>
	</div>
</section>