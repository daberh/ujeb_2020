<?php

$couleur_bootstrap_class = get_field('couleur',get_field('event_type'));
if($couleur_bootstrap_class === null) $couleur_bootstrap_class = 'secondary';
$bg_type_image = get_field('image',get_field('event_type'));
if($bg_type_image) $bg_type_image = 'style="background-image:url('.$bg_type_image['sizes']['medium'].')"';

$event_type_name = get_field('event_type')->name;

$start_date = get_field('start_date');
$end_date 	= get_field('end_date');
$start_time = get_field('start_time');
$end_time 	= get_field('end_time');

$season = (get_field('season'))?get_field('season'):'&nbsp;';

$discount_date 	= get_field('discount_date');
$discount 		= get_field('discount');
$price 			= get_field('price');

$state = get_field('etat');
$bad_states = [
	4 => get_field('camp_complet_filles'),
	5 => get_field('camp_complet_garcons'),
	6 => get_field('camp_complet')
];
$output = (array_key_exists(intval($state['value']), $bad_states)) ? $bad_states[intval($state['value'])]:false;

$min_age = get_field('min_age');
$max_age = get_field('max_age');
$from_age_of = get_field('from_age_of');

$directors = get_field('directors');
$contact_director = $directors ? $directors[0]->ID : null;

$place = get_field('place')->post_title;
$place_slug = get_field('place')->post_name;
$place_id = get_field('place')->ID;

$address_complete = get_field('address',$place_id);
$address = substr_replace($address_complete, '<br/>', strpos($address_complete, ',')+1, 0);




?>
<article class="col-lg-6 event event-<?= get_the_ID() ?>" data-aos="<?= get_query_var('animation'); ?>" data-aos-duration="<?= get_query_var('animation-time'); ?>">
	<div class="card mb-4 shadow-sm d-flex flex-row">
		<div class="card-header d-flex flex-column justify-content-center bg-<?= $couleur_bootstrap_class ?>" <?= $bg_type_image ?>>
			<?php if ($bg_type_image): ?><div class="blackground"></div><?php endif; ?>
			<h5 class="text-center my-0 font-weight-normal grandhotel text-white"><?= $season ?></h5>
			<h3 class="text-center my-0 font-weight-normal text-white"><?= $event_type_name ?></h3>
			<h4 class="text-center my-0 font-weight-normal grandhotel text-white">
				<?php if ($from_age_of): ?>
					à partir de <?= $min_age ?> ans
				<?php elseif($min_age&&$max_age): ?>
					de <?= $min_age ?> à <?= $max_age ?> ans
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			</h4>
		</div>
		<div class="card-body d-flex flex-column justify-content-between">
			<h1 class="card-title pricing-card-title text-center text-md-right text-<?= $couleur_bootstrap_class ?>">
				<?php
				if($price):
					echo $price.'€';
					if($discount_date&&$discount): ?>
					<small class="text-muted grandhotel">
						<small> -<?= $discount ?>€ *</small>
					</small><?php
					endif;
				else:
				?><small class="text-muted"><small><?= ___('infos à venir') ?></small></small>
				<?php
				endif;
				?>
			</h1>
			<div class="event-informations">
				<dl class="row small mb-1">
					<dt class="col-4 text-<?= $couleur_bootstrap_class ?>">Quand:</dt>
					<dd class="col-8">
					<?php
					if($end_date):
					?>
						<p>Du <?= $start_date ?> <?= $start_time ?></p>
						<p>Au <?= $end_date ?> <?= $end_time ?></p>
					<?php
					else: 
					?>
						<p>Le <?= $start_date ?> <?= $start_time ?></p>
					<?php
					endif;
					?></dd><?php
					if($place):
					?>
						<dt class="col-4 text-<?= $couleur_bootstrap_class ?>">Lieu:</dt>
						<dd class="col-8">
							<strong>
								<?php /*<a data-toggle="popover" data-html="true" data-placement="bottom" title="<?= $place ?>" data-content="<?= $address ?><br><a target='_blank' href='https://www.google.com/search?q=google+maps+<?= $address_complete ?>'>Plus d'informations</a>"><?= $place ?></a> */ ?>
								<a style="cursor:pointer;" data-toggle="modal" data-target="#<?= $place_slug ?>"><?= $place ?></a>
							</strong>
						</dd>
					<?php
					endif;
					if($directors):
					?>
						<dt class="col-4 text-<?= $couleur_bootstrap_class ?>">Direction:</dt>
						<dd class="col-8">
						<?php foreach($directors as $director): ?>
							<p><em><?= get_field('nom_a_afficher',$director->ID) ?></em></p>
						<?php endforeach; ?>
						</dd>
					<?php
					endif;
					?>
					</dl>
					<p class="text-right mb-1 small"><small>
					<?php
					if($discount_date&&$discount):
					?>
					* si payé avant le <?= $discount_date ?>
				<?php
				else: echo '&nbsp;';
				endif;
				?>
				</small></p>
			</div>
			<div class="d-flex justify-content-between event-buttons">
			<?php if($directors): ?>
				<button data-toggle="modal" data-target="#contact-directeur" data-campcat="<?= $event_type_name ?>" data-director="<?= $directors[0]->post_title ?>" data-directorid="<?= $contact_director ?>" type="button" class="btn btn-sm btn-outline-<?= $couleur_bootstrap_class ?>"><?= ___('Contact') ?></button>
			<?php else: ?>
				<a href="#contact" class="ujeb-links btn btn-sm btn-outline-<?= $couleur_bootstrap_class ?>"><?= ___('Contact') ?></a>
			<?php endif; ?>
			<?php $states_array = [ 2, 3, ]; ?>
			<?php if(!in_array(intval($state['value']), $states_array)): ?>
				<button data-toggle="modal" data-target="#preinscription" data-statemsg="<?= htmlentities($output) ?>" data-directorid="<?= $contact_director ?>" data-minage="<?= $min_age ?>" data-maxage="<?= $max_age ?>" data-eventid="<?= get_the_ID() ?>" type="button" class="btn btn-sm btn-outline-<?= $couleur_bootstrap_class ?>"><?= ___('préinscription') ?></button>
			<?php else: ?>
				<h4 class="text-right text-<?= $couleur_bootstrap_class ?> small align-self-center my-0">
					<?= $state['label'] ?>
				</h4>
			<?php endif; ?>
			</div>
		</div>
	</div>
</article>