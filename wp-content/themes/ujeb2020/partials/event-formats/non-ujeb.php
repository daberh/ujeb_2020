<?php
/*
$couleur_bootstrap_class = get_field('couleur',get_field('event_type'));
if($couleur_bootstrap_class === null) $couleur_bootstrap_class = 'secondary';
$bg_type_image = get_field('image',get_field('event_type'));
if($bg_type_image) $bg_type_image = 'style="background-image:url('.$bg_type_image['sizes']['medium'].')"';

$event_type_name = get_field('event_type')->name;

$start_date = get_field('start_date');
$end_date 	= get_field('end_date');
$start_time = get_field('start_time');
$end_time 	= get_field('end_time');

$season = (get_field('season'))?get_field('season'):'&nbsp;';

$min_age = get_field('min_age');
$max_age = get_field('max_age');
$from_age_of = get_field('from_age_of');

$title = get_field('nom_a_afficher');

$activity_content = get_field('description');

$organizer = get_field('organisateur');

$place = get_field('lieu')->post_title;
$place_slug = get_field('lieu')->post_name;
$place_id = get_field('lieu')->ID;

$showform = get_field('inscription');

?>
<article class="col-lg-6 event event-<?= get_the_ID() ?>">
	<div class="card mb-4 shadow-sm d-flex flex-row">
		<div class="card-header d-flex flex-column justify-content-center bg-<?= $couleur_bootstrap_class ?>" <?= $bg_type_image ?>>
			<?php if ($bg_type_image): ?><div class="blackground"></div><?php endif; ?>
			<h5 class="text-center my-0 font-weight-normal grandhotel text-white"><?= $season ?></h5>
			<h3 class="text-center my-0 font-weight-normal text-white"><?= $event_type_name ?></h3>
			<h4 class="text-center my-0 font-weight-normal grandhotel text-white">
				<?php if ($from_age_of): ?>
					à partir de <?= $min_age ?> ans
				<?php elseif($min_age&&$max_age): ?>
					de <?= $min_age ?> à <?= $max_age ?> ans
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			</h4>
		</div>
		<div class="card-body d-flex flex-column justify-content-between">
			<h1 class="card-title pricing-card-title text-center text-md-right text-<?= $couleur_bootstrap_class ?>">
				<small><small><?= $title ?></small></small>
			</h1>
			<div class="event-informations">
				<dl class="row small mb-1">
					<?php if($start_date): ?>
					<dt class="col-4 text-<?= $couleur_bootstrap_class ?>">Quand:</dt>
					<dd class="col-8">
					<?php if($end_date): ?>
						<p>Du <?= $start_date ?> <?= $start_time ?></p>
						<p>Au <?= $end_date ?> <?= $end_time ?></p>
					<?php else: ?>
						<p>Le <?= $start_date ?> <?= $start_time ?></p>
					<?php endif; ?>
					</dd>
					<?php endif; ?>
					<?php if($place): ?>
					<dt class="col-4 text-<?= $couleur_bootstrap_class ?>">Lieu:</dt>
					<dd class="col-8">
						<strong><a style="cursor:pointer;" data-toggle="modal" data-target="#<?= $place_slug ?>"><?= $place ?></a></strong>
					</dd>
					<?php endif; ?>
					<?php if($organizer): ?>
					<dt class="col-4 text-<?= $couleur_bootstrap_class ?>">Organisateur:</dt>
					<dd class="col-8">
						<p><em><?= $organizer ?></em></p>
					</dd>
					<?php endif; ?>
				</dl>
			</div>
			<div class="d-flex justify-content-end event-buttons">
				<div class="d-none activity-hidden-content"><?= $activity_content ?></div>
				<button data-toggle="modal" data-target="#contact-activity" data-title="<?= $title ?>" data-showform="<?= $showform ?>" type="button" class="btn btn-sm btn-outline-<?= $couleur_bootstrap_class ?>"><?= ___('En savoir plus') ?></button>
			</div>
		</div>
	</div>
</article>

<?php	*/ ?>