	<div class="modal fade" id="contact-directeur" tabindex="-1" role="dialog" aria-labelledby="contactDirecteur" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="contactDirecteur"><?= ___('Contacter la direction') ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body user-input">
					<?= get_field('bloc_texte_1_formulaire_contacter_direction_camp') ?>
				</div>
				<?= do_shortcode( '[contact-form-7 id="144" title="Contacter Direction Camp"]' , true) ?>
			</div>
		</div>
	</div>