<?php
$addresses = new WP_Query(['post_type' => 'addresses']);

if($addresses->have_posts()):
	while($addresses->have_posts()): $addresses->the_post();global $post; ?>
	<div class="modal fade" id="<?= $post->post_name ?>" tabindex="-1" role="dialog" aria-labelledby="address<?= ucfirst($post->post_name)?>" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="address<?= ucfirst($post->post_name)?>"><?= get_the_title() ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body user-input">
					<?= get_field('address') ?>
					<p>&nbsp;</p>
					<div class="googlemapssearch" data-search="<?= utf8_encode(get_field('address')) ?>" data-api-key="AIzaSyAo62aNUBbJc_jt3_4iDJDDW-ehJW2hxm0" width="100%" height="300" style="width:100%;height:300px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= ___('Fermer') ?></button>
				</div>
			</div>
		</div>
	</div>

<?php endwhile;
endif;
wp_reset_postdata();