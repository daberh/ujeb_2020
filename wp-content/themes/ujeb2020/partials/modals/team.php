	<div class="modal fade" id="devenir-equipier" tabindex="-1" role="dialog" aria-labelledby="devenirEquipier" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="devenirEquipier"><?= get_field('titre_devenir_equipier') ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body user-input">
					<?= get_field('devenir_equipier') ?>
				</div>
				<?= do_shortcode( '[contact-form-7 id="121" title="Devenir équipier"]' , true) ?>
			</div>
		</div>
	</div>