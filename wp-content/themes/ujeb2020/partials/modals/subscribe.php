	<div class="modal fade" id="preinscription" tabindex="-1" role="dialog" aria-labelledby="pre-inscription" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="pre-inscription"><?= ___('Pré-inscription') ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body user-input">
					<div class="event-card"></div>
					<div class="alert alert-danger" style="display: none;font-weight: bold;"></div>
				</div>
				<?= do_shortcode( '[contact-form-7 id="179" title="Formulaire de pré-inscription"]', true) ?>
			</div>
		</div>
	</div>