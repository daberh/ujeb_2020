	<div class="modal fade" id="contact-activity" tabindex="-1" role="dialog" aria-labelledby="contactActivity" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="contactActivity"><?#= ___('Contacter la direction') ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body user-input">
					&nbsp;
				</div>
				<div class="the-form">
					<?= do_shortcode( '[contact-form-7 id="144" title="Contacter Direction Camp"]' , true) ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= ___('Fermer') ?></button>
				</div>
			</div>
		</div>
	</div>