<section id="newsletter" class="" style="background-image: url(<?= get_field('image_de_fond_newsletter')['sizes']['ujeb-large'] ?>)">
	<div class="blackground"></div>
	<div class="container py-5" style="position:relative;z-index:3;">
		<header class="my-4">
			<h1 class="text-white text-center"><?= get_field('titre_newsletter') ?></h1>
		</header>
		<article class="pt-2 pb-4 text-center text-white">
			<?= get_field('bloc_texte_1_newsletter') ?>
			<?= do_shortcode( '[mc4wp_form id="127"]' ) ?>
		</article>

<?php /*

// FORMULAIRE PUR LE PLUGIN MAILCHIMP

<div class="form-row justify-content-center pt-5">
	<div class="col-sm-6 my-1">
		<input type="email" class="form-control" placeholder="Votre email">
	</div>
	<div class="col-sm-3 my-1">
		<input type="submit" class="btn btn-primary" value="s'abonner">
	</div>
</div>
<div class="form-row justify-content-center">
	<div class="form-group">
		<div class="my-1">
			<div class="form-check text-center">
				<label>
					<input name="AGREE_TO_TERMS" type="checkbox" value="1" required=""> J'ai lu et accepte <a href="http://ujeb.local/politique-de-confidentialite/" target="_blank" style="font-weight: bold;" class="text-white">les termes et les conditions</a>
				</label>
			</div>
		</div>
	</div>
</div>
*/ ?>
	</div>
</section>



