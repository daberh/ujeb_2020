	<div id="ujebCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="7000">
	<?php
	$slides = get_posts([
		'numberposts'=> 3,
		'post_type'=> 'slider'
	]);
	$nb_slides = count($slides);
	?>
	<?php if ($nb_slides > 1): $active = 'class="active"'; ?>
		<ol class="carousel-indicators">
		<?php for($nb = 0; $nb < $nb_slides; $nb++): ?>
			<li data-target="#ujebCarousel" data-slide-to="<?= $nb ?>" <?= $active ?>></li>
		<?php $active = ''; endfor; ?>
		</ol>
	<?php endif; ?>
		<div class="carousel-inner" role="listbox">
		<?php $active = "active"; foreach ($slides as $slide): setup_postdata( $slide ); ?>
			<div class="carousel-item parallax-background <?= $active ?>" style="background-image: url('<?= get_the_post_thumbnail_url( $slide->ID, 'ujeb-large' ) ?>')">
				<div class="blackground"></div>
				<div class="carousel-caption d-0none d-md-block">
					<h2 class="display-4"><?= get_the_title($slide->ID) ?></h2>
					<p class="lead"><?= get_field('subtitle',$slide->ID) ?></p>
					<?php if(get_field('bouton',$slide->ID)): $btn = get_field('bouton',$slide->ID); ?>
						<div class="text-center">
							<?php $pos = strpos($btn['url'],'#'); ?>
							<a href="<?= $btn['url'] ?>" target="<?= $btn['target'] ?>" class="btn btn-lg btn-primary <?= ($pos!==false)?'ujeb-links':'' ?>"><?= $btn['title'] ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php $active = ''; endforeach; wp_reset_postdata(); ?>
		</div>
	<?php if ($nb_slides > 1): ?>
		<a class="carousel-control-prev" href="#ujebCarousel" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only"><?= ___('Précédent') ?></span>
		</a>
		<a class="carousel-control-next" href="#ujebCarousel" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only"><?= ___('Suivant') ?></span>
		</a>
	<?php endif; ?>
	</div>