<?php

$about = [
	'titre_a_propos' 			=> get_field('titre_a_propos'),
	'bloc_texte_1' 				=> get_field('bloc_texte_1'),
	'img' 						=> get_field('bloc_image_1'),
	'bloc_texte_2' 				=> get_field('bloc_texte_2'),
	'bloc_texte_3' 				=> get_field('bloc_texte_3'),
	'titre_profession_de_foi' 	=> get_field('titre_profession_de_foi'),
	'titre_devenir_equipier' 	=> get_field('titre_devenir_equipier')
];

?>
<section class="py-5" id="about">
	<div class="container">
		<h1 class="text-center my-4"><?= $about['titre_a_propos'] ?></h1>
		<div class="row">
			<div class="col-md-6 user-input" data-aos="fade-right"><?= $about['bloc_texte_1'] ?></div>
			<div class="col-md-6 user-input" data-aos="fade-left">
			<?php if(filter_var($about['img']['sizes']['medium_large'], FILTER_VALIDATE_URL)): ?>
				<p><img class="img-fluid" src="<?= $about['img']['sizes']['medium_large'] ?>" alt="Le comité UJEB"></p>
			<?php else: ?>
				<p>&nbsp;</p>
			<?php endif; ?>
			</div>
			<div class="col-md-6 user-input" data-aos="fade-right"><?= $about['bloc_texte_2'] ?></div>
			<div class="col-md-6 user-input" data-aos="fade-left"><?= $about['bloc_texte_3'] ?></div>
		</div>
		<div class="row">
			<div class="col-6 text-center pt-4" data-aos="fade-right">
				<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#profession-foi">
					<?= $about['titre_profession_de_foi'] ?>
				</button>
			</div>
			<div class="col-6 text-center pt-4" data-aos="fade-left">
				<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#devenir-equipier">
					<?= $about['titre_devenir_equipier'] ?>
				</button>
			</div>
		</div>
	</div>
</section>
