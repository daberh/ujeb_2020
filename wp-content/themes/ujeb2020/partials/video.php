<section id="history" style="background-image: url(<?= get_field('image_de_fond_video')['sizes']['ujeb-large'] ?>)">
	<div class="blackground"></div>
	<div class="container py-5" style="position:relative;z-index:3;">
		<header class="my-4">
			<h1 class="text-white text-center"><?= get_field('titre_video') ?></h1>
		</header>
		<article class="pt-2 pb-4">
			<div style="padding:56.25% 0 0 0;position:relative;">
				<div class="vimeo_player" videoID="54083266" style="position:absolute;top:0;left:0;width:100%;height:100%;"></div>
			</div>
		</article>
	</div>
</section>