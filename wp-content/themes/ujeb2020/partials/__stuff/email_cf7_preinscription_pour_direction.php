<table style="max-width:600px;" align="center">
<tr>
<td><p style="text-align:center"><img src="https://ujeb.davidbernard.be/wp-content/themes/ujeb2020/media/ujeb-good-logo-carre-150x150.png" alt="Logo UJEB"></p></td>
</tr>
<tr>
<td>
<p>Bonjour,</p>
<br/><br/>
<p>Nous avons reçu une demande de pré-inscription pour votre camp.</p>
<br/><br/>
<p>Voici une copie des informations que nous avons reçu:</p>
<ul>
<li>Nom et Prénom du campeur : [your-name] [your-firstname]</li>
<li>Sexe : [your-sex]</li>
<li>Date de naissance : [_format_your-birthday "d-m-Y"]</li>
<li>E-mail: [your-email]</li>
<li>Adresse : [your-rue] [your-number] [your-box], [your-cp] [your-ville], [your-country]</li>
<li>Tel. contact 1 : [your-tel]</li>
<li>Tel. contact 2 : [your-tel2]</li>
<li>Tel. contact 3 : [your-tel3]</li>
<li>Nom du titulaire du compte qui paiera le camp : [your-paiement-name]</li>
<li>Avez-vous déjà participé à un camp UJEB : [your-camp]</li>
<li>Votre église : [your-church-name]</li>
<li>Commentaires : [your-message]</li>
</ul>
<br/>
<br/><br/><br/>
<p>À bientôt au camp.</p>
</td>
</tr>
<tr>
<td><br/><br/><br/><br/><br/><br/><p style="text-align:center"><small style="font-size: 10px">Cet email a été envoyé depuis le site <a href="https://ujeb.be">ujeb.be</a></small></p></td>
</tr>
</table>