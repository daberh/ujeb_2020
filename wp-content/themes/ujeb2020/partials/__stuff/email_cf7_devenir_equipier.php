<table style="max-width:600px;" align="center">
<tr>
<td><p style="text-align:center"><img src="https://ujeb.davidbernard.be/wp-content/themes/ujeb2020/media/ujeb-good-logo-carre-150x150.png" alt="Logo UJEB"></p></td>
</tr>
<tr>
<td>
<p>Reçu depuis le formulaire "devenir équipier" du site <a href="https://ujeb.be">ujeb.be</a></p>
<br/>
<br/>
<p>[your-name] ([your-email])</p>
<p>Message:</p>
<p>[your-message]</p>
</td>
</tr>
<tr>
<td><br/><br/><br/><br/><br/><br/><p style="text-align:center"><small style="font-size: 10px">Cet email a été envoyé depuis le site <a href="https://ujeb.be">ujeb.be</a></small></p></td>
</tr>
</table>