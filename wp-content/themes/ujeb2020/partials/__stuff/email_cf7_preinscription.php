<table style="max-width:600px;" align="center">
<tr>
<td><p style="text-align:center"><img src="https://ujeb.davidbernard.be/wp-content/themes/ujeb2020/media/ujeb-good-logo-carre-150x150.png" alt="Logo UJEB"></p></td>
</tr>
<tr>
<td>
<p>Bonjour,</p>
<br/><br/>
<p>Nous avons bien reçu votre demande de pré-inscription pour %%camptype%% se déroulant du %%startdate%% au %%enddate%%.</p>
<p>Celle-ci a été transmise au directeur du camp.</p>
<br/>
%%communication%%
<p>Afin de vous garantir une place à ce camp nous vous rappelons qu'<b>un campeur n'est considéré comme inscrit que s'il a effectué le payement du camp</b> (ou d’un acompte de 50 %).</p>
<p>Veuillez effectuer, dès que possible, le payement sur le compte <b>%%iban%%</b> (BIC : %%bic%%) en mentionnant les NOM et PRENOM du campeur en communication.</p>
<br/><br/>
<p>Voici une copie des informations que vous nous avez transmises:</p>
<ul>
<li>Nom et Prénom du campeur : [your-name] [your-firstname]</li>
<li>Sexe : [your-sex]</li>
<li>Date de naissance : [_format_your-birthday "d-m-Y"]</li>
<li>E-mail: [your-email]</li>
<li>Adresse : [your-rue] [your-number] [your-box], [your-cp] [your-ville], [your-country]</li>
<li>Tel. contact 1 : [your-tel]</li>
<li>Tel. contact 2 : [your-tel2]</li>
<li>Tel. contact 3 : [your-tel3]</li>
<li>Nom du titulaire du compte qui paiera le camp : [your-paiement-name]</li>
<li>Avez-vous déjà participé à un camp UJEB : [your-camp]</li>
<li>Votre église : [your-church-name]</li>
<li>Commentaires : [your-message]</li>
</ul>
<br/>
<p>Vous trouverez ci-dessous les liens vous permettant de télécharger quelques documents importants:</p> 
<br/>
<p>Fiche médicale:</p> 
<ul>
<li><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/FicheSanteJE.pdf">Juniors – Enfants</a></li>
<li><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/FicheSanteAJ.pdf">Ados – Jeunes</a></li>
</ul>
<br/>
<p>Règlement d’inscription et règlement d’ordre intérieur des camps UJEB:</p>
<ul>
<li><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/ROI_JE.pdf">Juniors – Enfants</a></li>
<li><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/ROI_AJ.pdf">Ados – Jeunes</a></li>
</ul>
<br/>
<br/>
<p>Pour contacter la direction du camp, veuillez utiliser l'adresse e-mail suivante: %%directormail%%</p>
<br/><br/><br/>
<p>À bientôt au camp.</p>
</td>
</tr>
<tr>
<td><br/><br/><br/><br/><br/><br/><p style="text-align:center"><small style="font-size: 10px">Cet email a été envoyé depuis le site <a href="https://ujeb.be">ujeb.be</a></small></p></td>
</tr>
</table>