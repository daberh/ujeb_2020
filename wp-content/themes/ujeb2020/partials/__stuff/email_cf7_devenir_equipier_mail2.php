<table style="max-width:600px;" align="center">
<tr>
<td><p style="text-align:center"><img src="https://ujeb.davidbernard.be/wp-content/themes/ujeb2020/media/ujeb-good-logo-carre-150x150.png" alt="Logo UJEB"></p></td>
</tr>
<tr>
<td>
<p>Bonjour,</p>
<br/>
<br/>
<p>Merci pour votre intérêt à devenir équipier pour l'UJEB.</p>
<br/>
<p>Voici les informations que vous nous avez communiqué:</p>
<ul>
<li>[your-name]</li>
<li>[your-email]</li>
<li>[your-message]</li>
<li>[acceptance-991]</li>
</ul>
<br/>
<br/>
<p>Nous reprendrons contact avec vous dans les plus bref délai.</p>
<br/>
<br/>
<br/>
<p>Le comité UJEB.</p>
</td>
</tr>
<tr>
<td><br/><br/><br/><br/><br/><br/><p style="text-align:center"><small style="font-size: 10px">Cet email a été envoyé depuis le site <a href="https://ujeb.be">ujeb.be</a></small></p></td>
</tr>
</table>