<section id="contact" class="py-5">
	<div class="container" data-aos="fade-right">
		<h1 class="my-4 text-center"><?= get_field('titre_contact') ?></h1>
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="user-input pt-4 pb-4">
					<?= get_field('bloc_texte_1_contact') ?>
				</div>
			</div>
			<div class="col-lg-8">
				<?= do_shortcode( '[contact-form-7 id="5" title="Formulaire de contact"]' , true) ?>
			</div>
		</div>
	</div>
</section>