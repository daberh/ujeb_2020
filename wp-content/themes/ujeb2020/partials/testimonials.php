<section id="testimonials" style="background-image: url(<?= get_field('image_de_fond_temoignages')['sizes']['ujeb-large'] ?>)">
	<h1 class="title testimonials-title">Témoignages</h1>
	<div class="blackground"></div>
	<div id="ujebTestimonials" class="carousel slide" data-ride="carousel">
	<?php $slides = get_posts(['numberposts'=> 5,'post_type'=> 'testimonials']);$nb_slides = count($slides);?>
	<?php if ($nb_slides > 1): $active = 'class="active"'; ?>
		<ol class="carousel-indicators">
		<?php for($nb = 0; $nb < $nb_slides; $nb++): ?>
			<li data-target="#ujebTestimonials" data-slide-to="<?= $nb ?>" <?= $active ?>></li>
		<?php $active = ''; endfor; ?>
		</ol>
	<?php endif;?>
		<div class="carousel-inner d-flex" role="listbox" style="height: 500px">
	<?php
	$active = "active";
	foreach ($slides as $slide): setup_postdata( $slide ); ?>
			<div class="carousel-item <?= $active ?>" style="height:auto;">
				<div class="carousel-caption d-0none d-md-block">
					<p class="lead"><?= get_field('temoignage',$slide->ID) ?></p>
					<h2 class="grandhotel"><?= get_the_title($slide->ID) ?></h2>
				</div>
			</div>
		<?php
		$active = '';
	endforeach;
	wp_reset_postdata();
	?>
		</div>
	<?php if ($nb_slides > 1): ?>
		<a class="carousel-control-prev" href="#ujebTestimonials" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only"><?= ___('Précédent') ?></span>
		</a>
		<a class="carousel-control-next" href="#ujebTestimonials" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only"><?= ___('Suivant') ?></span>
		</a>
	<?php endif; ?>
	</div>
</section>