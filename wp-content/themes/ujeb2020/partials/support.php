
<section class="py-5">
	<div class="container">
		<h1 class="my-4 text-center"><?= get_field('titre_nous_soutenir') ?></h1>
		<div class="user-input" data-aos="fade-right">
			<?= get_field('bloc_texte_1_nous_soutenir') ?>
		</div>
		<div class="user-input pt-4 pb-4" data-aos="fade-left">
			<?= get_field('bloc_texte_2_nous_soutenir') ?>
		</div>
		<div class="row">
			<article class="col-md-6" data-aos="fade-right">
				<div class="user-input">
					<?= get_field('bloc_texte_3_nous_soutenir') ?>
				</div>
			</article>
			<article class="col-md-6" data-aos="fade-left">
				<div class="user-input">
					<?= get_field('bloc_texte_4_nous_soutenir') ?>
				</div>
			</article>
			<article class="col-md-6" data-aos="fade-right">
				<div class="user-input">
					<?= get_field('bloc_texte_5_nous_soutenir') ?>
				</div>
			</article>
			<article class="col-md-6" data-aos="fade-left">
				<div class="user-input">
					<?= get_field('bloc_texte_6_nous_soutenir') ?>
				</div>
			</article>
			<article class="col-12" data-aos="fade-top">
				<h1 class="text-center my-4"><?= get_field('merci') ?></h1>
			</article>
		</div>
	</div>
</section>