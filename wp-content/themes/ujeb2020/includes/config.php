<?php
function ujeb2020_display_preloader(){
	return false;
	// return true;
}

function ujeb2020_get_primary_nav_menu_id(){
	return 5;
}

function ujeb2020_get_director_contact_form_id(){
	return 144;
}

function ujeb2020_get_subscription_contact_form_id(){
	return 179;
}

function ujeb2020_get_team_contact_form_id(){
	return 121;
}

function ujeb2020_get_convention_term_id(){
	return [9,10,11];
}

function ujeb2020_get_front_page_id(){
	return 2;
}

function ujeb2020_get_privacy_policy_page_id(){
	return 3;
}

function ujeb2020_get_ujeb_email_address($name = 'info'){
	$addresses = [
		'info' => 'daber1981@gmail.com', // mailssite@ujeb.be
		'director' => 'daber1981@gmail.com', // mailssite@ujeb.be
		'subscription' => 'daber1981@gmail.com', // mailssite@ujeb.be
	];

	return $addresses[$name];
}