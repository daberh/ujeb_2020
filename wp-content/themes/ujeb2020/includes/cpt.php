<?php

add_action('init', function(){

	$labels_events = [
		'name'                  => ___( 'Événements' ),
		'singular_name'         => ___( 'Événement' ),
		'menu_name'             => ___( 'Événements' ),
		'name_admin_bar'        => ___( 'Événements' ),
		'archives'              => ___( 'Événements Archives' ),
		'attributes'            => ___( 'Événements Attributes' ),
		'parent_item_colon'     => ___( 'Parent Item:' ),
		'all_items'             => ___( 'Les événements' ),
		'add_new_item'          => ___( 'Ajouter Événement' ),
		'add_new'               => ___( 'Ajouter' ),
		'new_item'              => ___( 'Nouveau' ),
		'edit_item'             => ___( 'Éditer' ),
		'update_item'           => ___( 'Mettre à jour' ),
		'view_item'             => ___( 'Voir l\'Événement' ),
		'view_items'            => ___( 'Voir les Événements' ),
		'search_items'          => ___( 'Rechercher' ),
		'not_found'             => ___( 'Not found' ),
		'not_found_in_trash'    => ___( 'Not found in Trash' ),
		'featured_image'        => ___( 'Featured Image' ),
		'set_featured_image'    => ___( 'Set featured image' ),
		'remove_featured_image' => ___( 'Remove featured image' ),
		'use_featured_image'    => ___( 'Use as featured image' ),
		'insert_into_item'      => ___( 'Insert into item' ),
		'uploaded_to_this_item' => ___( 'Uploaded to this item' ),
		'items_list'            => ___( 'Items list' ),
		'items_list_navigation' => ___( 'Items list navigation' ),
		'filter_items_list'     => ___( 'Filter items list' ),
	];
	$args_events = [
		'label'                 => ___( 'Événement' ),
		'description'           => ___( 'événements ujeb' ),
		'labels'                => $labels_events,
		'supports'              => [ 'title' , 'post-formats' ],
		'taxonomies'            => [ 'events_category' ],
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',//get_stylesheet_directory_uri() .'/media/admin-ujeb-menu.png',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false, // no single view
		'capability_type'       => 'post',
		'rewrite'      			=> [
			'slug' => 'agenda',
			'with_front' => false
		]
	];
	register_post_type( 'ujeb_events', $args_events );
	register_taxonomy(
		'events_category',
		'ujeb_events',
		[
			'label' 		=> ___("Types d'événement"),
			'labels' 		=> [
				'name' 			=> ___('Types'),
				'singular_name' => ___('Type'),
				'all_items' 	=> ___('Tous les types'),
				'edit_item' 	=> ___('Éditer le type'),
				'view_item' 	=> ___('Voir le type'),
				'update_item' 	=> ___('Mettre à jour le type'),
				'add_new_item' 	=> ___('Ajouter un type'),
				'new_item_name' => ___('Nouveau type'),
				'search_items' 	=> ___('Rechercher parmi les types'),
				'popular_items' => ___('Types les plus utilisés')
			],
			'hierarchical' 	=> false,
			// 'rewrite'      => [ 'slug' => 'agenda/evenements', 'with_front' => false ],
			'show_admin_column' => false,
		]
	);

	/**
	 * TEAMS
	 */	
	$labels_teams = [
		'name'               => ___( 'Directeurs' ),
		'singular_name'      => ___( 'Directeur' ),
		'add_new'            => ___( 'Ajouter un Directeur' ),
		'add_new_item'       => ___( 'Ajouter un Directeur' ),
		'edit_item'          => ___( 'Modifier un Directeur' ),
		'new_item'           => ___( 'Nouveau Directeur' ),
		'view_item'          => ___( 'Voir le Directeur' ),
		'search_items'       => ___( 'Rechercher dans les Directeurs' ),
		'not_found'          => ___( 'No Directeurs found' ),
		'not_found_in_trash' => ___( 'No Directeurs found in Trash' ),
		'parent_item_colon'  => ___( 'Parent Directeur:' ),
		'menu_name'          => ___( 'Directeurs' ),
	];
	
	$args_teams = [
		'labels'              => $labels_teams,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => [],
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-businessperson',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => false, // no single view
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => [
			'title',
			// 'editor',
			// 'thumbnail',
		],
	];
	register_post_type( 'directors', $args_teams );


	$labels_addresses = [
		'name'               => ___( 'Adresses' ),
		'singular_name'      => ___( 'Adresse' ),
		'add_new'            => ___( 'Ajouter une nouvelle adresse' ),
		'add_new_item'       => ___( 'Ajouter une nouvelle adresse' ),
		'edit_item'          => ___( "Modifier l'adresse" ),
		'new_item'           => ___( 'Nouvelle adresse' ),
		'view_item'          => ___( "Voir l'adresse" ),
		'search_items'       => ___( 'Rechercher une adresse' ),
		'not_found'          => ___( 'No Adresses found' ),
		'not_found_in_trash' => ___( 'No Adresses found in Trash' ),
		'parent_item_colon'  => ___( 'Parent Adresse:' ),
		'menu_name'          => ___( 'Adresses' ),
	];
	$args_addresses = [
		'labels'              => $labels_addresses,
		'hierarchical'        => false,
		'description'         => 'Events addresses',
		'taxonomies'          => [],
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-site-alt',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => false,
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => [
			'title'
		],
	];
	register_post_type( 'addresses', $args_addresses );
	
	

	/**
	 * SLIDER
	 */

	
	$labels_slider = [
		'name'               => ___( 'Slider' ),
		'singular_name'      => ___( 'Slide' ),
		'add_new'            => ___( 'Add New Slide' ),
		'add_new_item'       => ___( 'Add New Slide' ),
		'edit_item'          => ___( 'Edit Slide' ),
		'new_item'           => ___( 'New Slide' ),
		'view_item'          => ___( 'View Slide' ),
		'search_items'       => ___( 'Search Slider' ),
		'not_found'          => ___( 'No Slider found' ),
		'not_found_in_trash' => ___( 'No Slider found in Trash' ),
		'parent_item_colon'  => ___( 'Parent Slide:' ),
		'menu_name'          => ___( 'Slider' ),
	];
	
	$args_slider = [
		'labels'              => $labels_slider,
		'hierarchical'        => false,
		'description'         => '',
		'taxonomies'          => [],
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-slides',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => false,
		'exclude_from_search' => true,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => [
			'title',
			'thumbnail',
		],
	];
	register_post_type( 'slider', $args_slider );

	$labels_testimonials = [
		'name'               => ___( 'Témoignages'),
		'singular_name'      => ___( 'Témoignage'),
		'add_new'            => ___( 'Ajouter Témoignage'),
		'add_new_item'       => ___( 'Ajouter Témoignage'),
		'edit_item'          => ___( 'Modifier Témoignage'),
		'new_item'           => ___( 'Nouveau Témoignage'),
		'view_item'          => ___( 'Voir le Témoignage'),
		'search_items'       => ___( 'Rechercher Témoignages'),
		'not_found'          => ___( 'No Témoignages found'),
		'not_found_in_trash' => ___( 'No Témoignages found in Trash'),
		'parent_item_colon'  => ___( 'Parent Témoignage:'),
		'menu_name'          => ___( 'Témoignages'),
	];
	$args_testimonials = [
		'labels'              => $labels_testimonials,
		'hierarchical'        => false,
		'description'         => '',
		'taxonomies'          => [],
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-testimonial',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => false,
		'exclude_from_search' => true,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => [
			'title',
			// 'editor',
			// 'thumbnail'
		],
	];
	register_post_type( 'testimonials', $args_testimonials );

}, 0 );