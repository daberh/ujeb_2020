<?php

add_action('after_setup_theme',function(){
	// add_theme_support( 'automatic-feed-links' );
	load_theme_textdomain('ujeb2020');
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('align-wide');
	add_theme_support('menus');
	add_image_size('ujeb-large',1200);
});

add_action('wp_enqueue_scripts',function(){
	wp_dequeue_style('wp-block-library');
	wp_dequeue_style('wp-block-library-theme');

	wp_enqueue_style('google-fonts','https://fonts.googleapis.com/css?family=Grand+Hotel|Luckiest+Guy|Open+Sans&display=swap',[],false,'all');
	wp_enqueue_style('bootstrap-style',get_stylesheet_directory_uri().'/assets/css/bootstrap.min.css',[],false,'all');
	wp_enqueue_style('fontawesome',get_stylesheet_directory_uri().'/assets/css/all.min.css',['bootstrap-style'],false,'all');
	wp_enqueue_style('ujeb-style',get_stylesheet_directory_uri().'/assets/css/ujeb.css',['fontawesome'],false,'all');
	
	// wp_enqueue_script('tarteaucitron',get_stylesheet_directory_uri().'/assets/js/tarteaucitron/tarteaucitron.js',[],false,true);
	// wp_enqueue_script('popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',['jquery'],false,true);
	wp_enqueue_script('bootstrap',get_stylesheet_directory_uri().'/assets/js/bootstrap.bundle.min.js',['jquery'],false,true);
	wp_enqueue_script('jquery-match-height',get_stylesheet_directory_uri().'/assets/js/jquery-match-height.min.js',['jquery'],false,true);
	wp_enqueue_script('ujeb-script',get_stylesheet_directory_uri().'/assets/js/ujeb.js',['jquery'],false,true);
});




function wpse_enqueue_datepicker() {
    // Load the datepicker script (pre-registered in WordPress).
    wp_enqueue_script( 'jquery-ui-datepicker' );

    // You need styling for the datepicker. For simplicity I've linked to the jQuery UI CSS on a CDN.
    wp_register_style( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' );  
}
add_action( 'wp_enqueue_scripts', 'wpse_enqueue_datepicker' );