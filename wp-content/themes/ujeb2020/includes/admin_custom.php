<?php

// custom login page
add_action('login_enqueue_scripts', function(){

	/**
	 * CONFIG
	 */
	$the_color = '#DD5449';
	$the_image 		= [
		'url'		=> get_stylesheet_directory_uri().'/media/ujeb-good-logo.png',
		'width'		=> '300',
		'height'	=> '194'
	];

?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(<?= $the_image['url'] ?>);
			height: <?= $the_image['height'].'px'; ?>;
			width: <?= $the_image['width'].'px' ?>;
			background-size: <?= $the_image['width'].'px '. $the_image['height'].'px'; ?>;
			background-repeat: no-repeat;
			background-position: center center;
			/*padding-bottom: 30px;*/
			/*margin : 0 !important;*/
			color: <?= $the_color ?>;
			/*filter: invert(100%);*/
		}
		.login #login_error, .login .message {
			border-left: 4px solid <?= $the_color ?> !important;
		}
		#loginform #wp-submit {
			background: <?= $the_color ?> !important;
			border-color: <?= $the_color ?> !important;
			color: #fff;
			box-shadow: 0 1px 0 <?= $the_color ?>;
			text-shadow: 0 -1px 1px <?= $the_color ?>, 1px 0 1px <?= $the_color ?>, 0 1px 1px <?= $the_color ?>, -1px 0 1px <?= $the_color ?>;
		}
		<?php /* focused */ ?>
		input[type=text]:focus,input[type=checkbox]:focus,input[type=password]:focus{
			border-color: <?= $the_color ?> !important;
			box-shadow: 0 0 0 1px <?= $the_color ?> !important;
		}
		<?php /* input password eye */ ?>
		.wp-core-ui .button-secondary{
			color: <?= $the_color ?> !important;
		}
	</style>
	<?php 
});
add_filter( 'login_headerurl', function(){return home_url();});
add_filter( 'login_headertext', function(){return ' ';});



/**
 * CUSTOM STYLE IN ADMIN
 * hide #tagsdiv-events_category in body.post-type-ujeb_events
 */
add_action( 'admin_footer', function(){
	echo '
	<style>
		/* body.post-type-ujeb_events #tagsdiv-events_category{display:none !important;} */
		.dashicons-businessperson::before,
		.dashicons-admin-site-alt::before,
		.dashicons-slides::before,
		.dashicons-testimonial::before,
		.dashicons-calendar-alt::before{
			color:#DD5449 !important;
		}
	</style>';

});




// Admin Notices = Tutorials
// function general_admin_notice(){
// 	global $pagenow;
// 	if ( $pagenow == 'options-general.php' ) {
// 		echo '<div class="notice notice-warning is-dismissible">
// 			<p>This notice appears on the settings page.</p>
// 		</div>';
// 	}
// }
// add_action('admin_notices', 'general_admin_notice');

add_action('admin_notices',function(){
	global $pagenow, $post_type, $post;
	// if ( ($pagenow == 'post.php'||$pagenow == 'post-new.php'||$pagenow == 'edit.php') && $post_type == 'directors' ) {
	// 	echo '<div class="notice notice-info is-dismissible">
	// 		<p><b>Pour un meilleur affichage (et SEO), les noms de famille ne doivent PAS être en majuscules, sauf la première lettre</b></p>
	// 	</div>';
	// }
	if ( $pagenow == 'post.php' && $_GET['post'] == 2 ) {
		echo '<div class="notice notice-info is-dismissible">
			<p><b>L\'éditeur juste avant Yoast SEO doit contenir le texte important pour le référencement</b></p>
		</div>';
	}
});







add_action( 'after_setup_theme',function(){
     add_theme_support( 'post-formats', [ 'aside', 'gallery' ] ); // ,'link'
}, 11 );

add_filter('gettext_with_context',function($translation, $text, $context, $domain){
    $names = [
    	'Standard'	=> 'Vide (ne sert à rien)',
        'Aside'  	=> 'Camp UJEB',
        'Gallery' 	=> 'Activité',
        // 'Link' 		=> 'Non UJEB'
    ];
    if ($context == 'Post format')
        $translation = str_replace(array_keys($names), array_values($names), $text);
    return $translation;
}, 10, 4);






/**
* add order column to admin listing screen
*/
add_action('manage_edit-ujeb_events_columns',function($ujeb_events) {
	$ujeb_events['ujeb_events'] = ___(' ');
	return $ujeb_events;
});

/**
* show custom order column values
*/
add_action('manage_ujeb_events_posts_custom_column',function($name){
	global $post;

	switch ($name) {
		case 'ujeb_events':
			$order = get_post_format_string( get_post_format($post->ID));
			echo $order;
			break;
	   	default:
			break;
	}
});

/**
* make column sortable
*/
// add_filter('manage_edit-ujeb_events_sortable_columns',function($columns){
// 	$columns['ujeb_events'] = 'ujeb_events';
// 	return $columns;
// });


/**
* remove column sortable
*/
function ujeb2020_my_manage_columns( $columns ) {
	unset($columns['date']);
	return $columns;
}
function ujeb2020_my_column_init() {
	add_filter( 'manage_ujeb_events_posts_columns' , 'ujeb2020_my_manage_columns' );
}
add_action( 'admin_init' , 'ujeb2020_my_column_init' );