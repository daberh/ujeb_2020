<?php

add_action('wpcf7_before_send_mail', function($WPCF7_ContactForm){

	// CONTACT DIRECTOR
	if (ujeb2020_get_director_contact_form_id() == $WPCF7_ContactForm->id()) {
		$currentformInstance  = WPCF7_ContactForm::get_current();
		$contactformsubmition = WPCF7_Submission::get_instance();
		if ($contactformsubmition){
			$data = $contactformsubmition->get_posted_data();
			if (empty($data))
				return;
			$mail = $currentformInstance->prop('mail');
			$cc_email = [];
			$director_email = get_field('email',$data['director-id']);
			$email_organisateur = $data['email_organisateur'];

			// // Quand LIVE
			if(!empty($director_email))
				$mail['recipient'] = $director_email;
			if(!empty($email_organisateur))
				$mail['recipient'] = $email_organisateur;


			// UJEB MAIL FOR COPY
			array_push($cc_email, ujeb2020_get_ujeb_email_address('director'));

			$cclist = implode(', ',$cc_email);
			if(!empty($cclist))
				$mail['additional_headers'] = "Cc: $cclist";
			if(!empty($data['your-email']))
				$mail['additional_headers'] .= "\r\n Reply-to: ".$data['your-email'];

// ob_start();
// var_dump($data,$mail);
// $mail['body'] = ob_get_clean();

			$currentformInstance->set_properties(["mail" => $mail]);
			return $currentformInstance;
		}
	}

	// SUBSCRIPTION
	if (ujeb2020_get_subscription_contact_form_id() == $WPCF7_ContactForm->id()) {
		$currentformInstance  = WPCF7_ContactForm::get_current();
		$contactformsubmition = WPCF7_Submission::get_instance();
		if ($contactformsubmition){
			$data = $contactformsubmition->get_posted_data();
			if (empty($data))
				return;
			$mail = $currentformInstance->prop('mail');

			// $cc_email = [];

			$directorid = $data['directorid'];
			$director_email = get_field('email',$directorid);
			$director_iban = get_field('iban',$directorid);
			$director_bic = get_field('bic',$directorid);

			$campid = $data['campid'];
			$camp_etat = intval(get_field('etat',$campid)['value']);
			$communication = '';
			if( $camp_etat == 4)
				$communication = '<p><b style="color:#ff0000">'.get_field('camp_complet_filles',$campid).'</b></p><br/>';
			if( $camp_etat == 5)
				$communication = '<p><b style="color:#ff0000">'.get_field('camp_complet_garcons',$campid).'</b></p><br/>';
			if( $camp_etat == 6)
				$communication = '<p><b style="color:#ff0000">'.get_field('camp_complet',$campid).'</b></p><br/>';
			$startdate = get_field('start_date',$campid);
			$enddate = get_field('end_date',$campid);

			$camptype = get_field('event_type',$campid)->name;


			// alter body
			$body = $mail['body'];
			$body = str_replace('%%directormail%%','<a href="mailto:'.$director_email.'">'.$director_email.'</a>',$body);
			$body = str_replace('%%iban%%',$director_iban,$body);
			$body = str_replace('%%bic%%',$director_bic,$body);
			$body = str_replace('%%camptype%%',$camptype,$body);
			$body = str_replace('%%communication%%',$communication,$body);
			$body = str_replace('%%startdate%%',$startdate,$body);
			$body = str_replace('%%enddate%%',$enddate,$body);

			$mail['body'] = $body;
			$mail['subject'] = 'UJEB.BE - Préinscription '.$camptype.' du '.$startdate.' au '.$enddate.' de '.$data['your-name'].' '.$data['your-firstname'];
			
			$mail['additional_headers'] .= "\r\n Reply-to: ".$director_email;

			$currentformInstance->set_properties(["mail" => $mail]);
			return $currentformInstance;
		}
	}
});

// // POUR LE MAIL 2 DE PRE INSCRIPTION (pour la direction)
add_filter('wpcf7_additional_mail', function($additional_mail, $WPCF7_ContactForm) {
	if (ujeb2020_get_subscription_contact_form_id() == $WPCF7_ContactForm->id()) {

		$submission = WPCF7_Submission::get_instance();
		$wpcf7      = WPCF7_ContactForm::get_current();
		$data 		= $submission->get_posted_data();
		
		$body 		= $additional_mail['mail_2']['body'];

		$campid 	= $data['campid'];
		$startdate 	= get_field('start_date',$campid);
		$camptype 	= get_field('event_type',$campid)->name;

		$body = str_replace('%%typecamp%%',$camptype,$body);
		$body = str_replace('%%startdate%%',$startdate,$body);

		$additional_mail['mail_2']['body'] = $body;
		
		$director_email = get_field('email',$data['directorid']);

		$additional_mail['mail_2']['subject'] = 'UJEB.BE - Préinscription '.$camptype.' du '.$startdate.' au '.$enddate.' de '.$data['your-name'].' '.$data['your-firstname'];

		if(!empty($director_email))
			$additional_mail['mail_2']['recipient'] = $director_email;

		return $additional_mail;
	}
}, 10, 2);









/*
// ob_start();
// var_dump($data,$mail);
// $mail['body'] = ob_get_clean();
// var_dump($data,$mail);

array(12) {
  ["_wpcf7"]=>  string(3) "144"
  ["_wpcf7_version"]=>  string(5) "5.1.7"
  ["_wpcf7_locale"]=>  string(5) "fr_FR"
  ["_wpcf7_unit_tag"]=>  string(13) "wpcf7-f144-o3"
  ["_wpcf7_container_post"]=>  string(1) "0"
  ["your-name"]=>  string(7) "Bernard"
  ["your-email"]=>  string(19) "david@adrenaline.be"
  ["your-message"]=>  string(15) "Mon body test 2"
  ["acceptance-777"]=>  string(1) "1"
  ["your-website"]=>  string(0) ""
  ["director-id"]=>  string(3) "125"
  ["mc4wp_checkbox"]=>  string(3) "Non"
}

array(9) {
  ["active"]=>  bool(true)
  ["subject"]=>  string(31) "Formulaire de contact directeur"
  ["sender"]=>  string(38) "UJEB.be <website@ujeb.davidbernard.be>"
  ["recipient"]=>  string(21) "hello@davidbernard.be"
  ["body"]=>  string(70) "Bernard
david@adrenaline.be
Mon body test 2
Autorisé : Vous acceptez notre politique de confidentialité
125"
  ["additional_headers"]=>  string(55) "Cc: daber1981@gmail.com
 Reply-to: david@adrenaline.be"
  ["attachments"]=>  string(0) ""
  ["use_html"]=>  bool(false)
  ["exclude_blank"]=>  bool(false)
}

*/







