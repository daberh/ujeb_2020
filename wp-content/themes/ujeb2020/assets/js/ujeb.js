jQuery(document).ready(function($) {

	var loader = setTimeout(goodbyeloader(), 2000);
	function goodbyeloader(){
		$('.preloader').fadeOut(500);
		clearTimeout(loader);
	}

	$('.event-informations').matchHeight();
	$('.event .card').matchHeight();

	$('[name="your-birthday"]').datepicker({
		dateFormat: 'yy-mm-dd',
	});

	var scrollTop = 0,
		windowWidth = 0;
	$(window).scroll(function(){
		scrollTop = $(window).scrollTop();
		windowWidth = $(window).width();
		if (windowWidth<768) return;
		if (scrollTop >= 10){
			$('body').addClass('scrolled');
			$('.follow-us-top-nav').hide();
		} else if (scrollTop < 10) {
			$('body').removeClass('scrolled');
			$('.follow-us-top-nav').show();
		}
	});
	var $el = $('.parallax-background');
    $(window).on('scroll', function () {
        var scroll = $(document).scrollTop();
        $el.css({
            'background-position':'50% '+(-.4*scroll)+'px'
        });
    });

	$('.real-nav .nav-link[href*="#"], .ujeb-links').on('click',function(){
		var url = $(this).attr('href'),
			anchor = url.substring(url.indexOf("#")),
			speed = 750;
		if (!url.includes("#"))
			return false;
		$('html, body').animate({scrollTop:$(anchor).offset().top-100},speed);
		return false;
	});

	$('#preinscription, #contact-directeur, #contact-activity').on('show.bs.modal',function(event){
		var modal = $(this),
			the_id = modal.attr('id'),
			button = $(event.relatedTarget);
		
		if (the_id == 'contact-directeur'){
			var contactDirecteur = button.data('director'),
				campCat = button.data('campcat'),
				directeurId = button.data('directorid');
			modal.find('.modal-title').html(
				'Contacter la direction ' + campCat + '<br/><span class="grandhotel">' + contactDirecteur + '</span>'
			);
			modal.find('.modal-content form input#directorId').val(directeurId);
		}
		if (the_id == 'contact-activity'){
			var description = button.parent().find('.activity-hidden-content').html(),
				form_or_not = button.data('showform'),
				email_organisateur = button.data('email');
			modal.find('.user-input').html(description);
			modal.find('.modal-title').html(button.data('title'));

			if(form_or_not == false){
				modal.find('.the-form').hide();
				modal.find('.modal-content .modal-footer').show();
			}
			else{
				modal.find('#email_organisateur').val(email_organisateur);
				modal.find('.the-form').show();
				modal.find('.modal-content>.modal-footer').hide();
				// modal.find('.the-form .modal-footer').show();
			}
		}
		if (the_id == 'preinscription'){
			var card = button.closest('.event'),
				ageMin = button.data('minage'),
				ageMax = button.data('maxage'),
				campId = button.data('eventid'),
				msg = button.data('statemsg'),
				dateMin = new Date(),
				dateMax = new Date(),
				directeurId = button.data('directorid');

			card.clone().removeClass('col-lg-6').addClass('col-12').appendTo('.modal-body .event-card');
			modal.find('.modal-content form input#directorid').val(directeurId);
			modal.find('.modal-content form input#eventid').val(campId);

			if (msg.length > 0)
				modal.find('.alert.alert-danger').text(msg).show();

			dateMin.setFullYear( dateMin.getFullYear() - (ageMin + 1) );
			dateMax.setFullYear( dateMax.getFullYear() - (ageMax + 1) );

			// modal.find('[name="your-birthday"]').attr({
			// 	max: dateMin.getFullYear()+'-'+("0" + (dateMin.getMonth() + 1)).slice(-2)+'-'+("0" + dateMin.getDate()).slice(-2),
			// 	min: dateMax.getFullYear()+'-'+("0" + (dateMax.getMonth() + 1)).slice(-2)+'-'+("0" + dateMax.getDate()).slice(-2)
			// });

			var max = dateMin.getFullYear()+'-'+("0" + (dateMin.getMonth() + 1)).slice(-2)+'-'+("0" + dateMin.getDate()).slice(-2);
			var min = dateMax.getFullYear()+'-'+("0" + (dateMax.getMonth() + 1)).slice(-2)+'-'+("0" + dateMax.getDate()).slice(-2);

			$('[name="your-birthday"]').datepicker('option', 'minDate', min );
			$('[name="your-birthday"]').datepicker('option', 'maxDate', max );
		}
	});

	$('#preinscription, #contact-directeur, #devenir-equipier, #contact-activity').on('hidden.bs.modal',function(event){
		var modal = $(this);
		modal.find('.wpcf7-response-output').empty();
		modal.find('.wpcf7-response-output').css('display','none');
		modal.find('.alert.alert-danger').hide();
		modal.find('.event-card').empty();
	});

	$('#devenir-equipier .wpcf7-response-output').appendTo($('#devenir-equipier .response')).addClass('text-center');
	$('#contact-directeur .wpcf7-response-output').appendTo($('#contact-directeur .response')).addClass('text-center');
	$('#preinscription .wpcf7-response-output').appendTo($('#preinscription .response')).addClass('text-center');
	$('#contact-activity .wpcf7-response-output').appendTo($('#contact-activity .response')).addClass('text-center');

	$('body').on('show.bs.modal', '.modal', function (e) {
		// fix the problem of ios modal form with input field
		var $this = $(this);
		if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
			// Position modal absolute and bump it down to the scrollPosition
			var heightModal = Math.max($('body').height(), $(window).height(), $(document).height()) + 1;
			$this.css({
				position: 'absolute',
				paddingTop: $(window).scrollTop() + 'px',
				height: heightModal + 'px'
			});
			// Position backdrop absolute and make it span the entire page
			//
			// Also dirty, but we need to tap into the backdrop after Boostrap 
			// positions it but before transitions finish.
			//
			setTimeout(function () {
				$('.modal-backdrop').css({
					position: 'absolute',
					top: 0,
					left: 0,
					width: '100%',
					height: heightModal + 'px'
				});
			}, 500);
		}
	});


});