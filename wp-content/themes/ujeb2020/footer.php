<footer id="footer">
	<div class="container py-5">
		<div class="row">

			<div class="col-12 col-md-4 align-self-center footer-logo">
				<a href="<?= get_home_url() ?>"><img style="width: 160px" src="<?= get_stylesheet_directory_uri() ?>/assets/media/logos/ujeb-logo.svg" class="logo-footer" alt="logo ujeb"></a>
			</div>
			
			<div class="col-12 col-md-8 align-self-center pt-4">
				<div class="navbar navbar-expand navbar-light">
					<ul class="navbar-nav ml-auto footer-social-nav">
						<li class="follow-us-text grandhotel">nous suivre </li>
						<?php
						$seo_data = get_option( 'wpseo_social' );
							if (!empty($seo_data['facebook_site'])) : ?><li class="nav-item"><a target="_blank" href="<?= $seo_data['facebook_site'] ?>" class="nav-link"><i class="fab fa-facebook fa-2x"></i></a></li><?php
							endif;
							if (!empty($seo_data['twitter_site'])) : ?><li class="nav-item"><a target="_blank" href="https://twitter.com/<?= $seo_data['twitter_site'] ?>" class="nav-link"><i class="fab fa-twitter fa-2x"></i></a></li><?php
							endif;
							if (!empty($seo_data['youtube_url'])) : ?><li class="nav-item"><a target="_blank" href="<?= $seo_data['youtube_url'] ?>" class="nav-link"><i class="fab fa-youtube fa-2x"></i></a></li><?php
							endif;
						?>
					</ul>
				</div>
			</div>

		</div>
		<div class="row pt-5">
			<div class="col-12 col-md-6 text-center">
				<p>Fiche médicale:</p>
				<p><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/FicheSanteJE.pdf" target="_blank">Juniors – Enfants</a></p>
				<p><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/FicheSanteAJ.pdf" target="_blank">Ados – Jeunes</a></p>
			</div>
			<div class="col-12 col-md-6 text-center">
				<p>Règlement d’inscription et règlement d’ordre intérieur des camps UJEB:</p>
				<p><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/ROI_JE.pdf" target="_blank">Juniors – Enfants</a></p>
				<p><a href="https://ujeb.davidbernard.be/wp-content/uploads/2020/07/ROI_AJ.pdf" target="_blank">Ados – Jeunes</a></p>
			</div>
		</div>
	</div>
	<div class="container copyright text-center">
		<p>©2020 UJEB.be - <a href="<?= get_privacy_policy_url() ?>"><?= get_the_title(ujeb2020_get_privacy_policy_page_id()) ?></a> - <a href="#tarteaucitron">Gestion&nbsp;des&nbsp;cookies</a></p>
	</div>
</footer>
<?php wp_footer() ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/AmauriC/tarteaucitron.js@1.4/tarteaucitron.js"></script>
<script type="text/javascript">
tarteaucitron.init({
	"privacyUrl": "<?= get_privacy_policy_url() ?>",
	"hashtag": "#tarteaucitron",
	"cookieName": "tarteaucitron",
	"orientation": "bottom",
	"showAlertSmall": false,
	"cookieslist": true,
	"adblocker": false,
	"AcceptAllCta" : true,
	"highPrivacy": true,
	"handleBrowserDNTRequest": false,
	"removeCredit": true,
	"moreInfoLink": true,
	"useExternalCss": false,
});
(tarteaucitron.job = tarteaucitron.job || []).push('vimeo');
(tarteaucitron.job = tarteaucitron.job || []).push('googlemapssearch');
</script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
	AOS.init();
</script>
</body>
</html>