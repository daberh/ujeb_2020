<?php // wp_redirect( home_url(), 301 ); exit; ?>
<?php get_header() ?>
<section class="d-flex justify-content-center align-items-center" style="height: 60vh">
	<div class="container text-center">
		<h1 class="display-4">404</h1>
		<p>Pas trouvé... ( on a un nouveau site ;-) )</p>
		<a href="<?= get_home_url() ?>" class="btn btn-primary btn-lg">Accueil</a>
	</div>
</section>
<?php get_footer() ?>