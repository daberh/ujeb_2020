<?php
get_header();

get_template_part('partials/events');
get_template_part('partials/newsletter'); 
get_template_part('partials/about');
get_template_part('partials/video');
get_template_part('partials/support');
get_template_part('partials/testimonials');
get_template_part('partials/contact');

get_template_part('partials/modals/faith');
get_template_part('partials/modals/team');
get_template_part('partials/modals/contact-director');
get_template_part('partials/modals/contact-activity');
get_template_part('partials/modals/subscribe');
get_template_part('partials/modals/addresses');

get_footer();
?>